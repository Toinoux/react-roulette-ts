import React from 'react';

import './App.scss';

// NB CARRE BLANC => VRAI CASE POUR LA MISE
const NB_VISIBLE_CELL_X = 5;
const NB_VISIBLE_CELL_Y = 5;

// NB CARRE CLIQUABLE POUR DETECTER LA POSITION DU CLIC DANS LE CARRE BLANC
const NB_DETECTABLE_CELL_PER_VISIBLE_CELL_X = 10;
const NB_DETECTABLE_CELL_PER_VISIBLE_CELL_Y = 10;

// EPASSEUR DE CARRE CLIQUABLE A PRENDRE EN COMPTE POUR LE CHEVAUCHEMENT LORS DE LA MISE
const MULTI_CELL_DETECTION_RANGE = 1

/**
 * - Fonction qui fait des maths et des logs
 * @param realX Index X du carre blanc cliqué (vrai carre pour la mise)
 * @param realY Index Y du carre blanc cliqué (vrai carre pour la mise)
 * @param detectedX Index X du carre cliquable dans le carre blanc (pour calculer le chevauchement)
 * @param detectedY Index Y du carre cliquable dans le carre blanc (pour calculer le chevauchement)
 */
const fonctionQuiFaitLesMaths = (realX: number, realY: number, detectedX: number, detectedY: number) => {
  const cellIndex = (realY * NB_VISIBLE_CELL_Y) + realX + 1;
  let affectedCells = [cellIndex],
      onTop, onBottom = false;
  console.log('CLICKED CELL:', cellIndex);

  // SI CLIQUE SUR PETIT CARRE AVEC detectedX == 1
  if (detectedX === 0) {
    // ALORS CLIQUE DETECTER ENTRE LA CASE CLIQUE ET CELLE AU DESSUS
    console.log('clique detecte en haut de case', cellIndex - NB_VISIBLE_CELL_X, 'et', cellIndex, '(ligne', realY, 'et', realY + 1 + ')');
    affectedCells.push(cellIndex - NB_VISIBLE_CELL_X);
    onTop = true;
  }

  // SI CLIQUE SUR PETIT CARRE AVEC detectedX >= 9
  if (detectedX >= NB_DETECTABLE_CELL_PER_VISIBLE_CELL_X - MULTI_CELL_DETECTION_RANGE) {
    // ALORS CLIQUE DETECTER ENTRE LA CASE CLIQUE ET CELLE EN DESSOUS
    console.log('clique detecte bas de case', cellIndex, 'et', cellIndex + NB_VISIBLE_CELL_X, '(ligne', realY + 1, 'et', realY + 2 + ')');
    affectedCells.push(cellIndex + NB_VISIBLE_CELL_X);
    onBottom = true;
  }

  // SI CLIQUE SUR PETIT CARRE AVEC detecteY == 0
  if (detectedY === 0) {
    // ALORS CLIQUE DETECTER ENTRE LA CASE CLIQUE ET CELLE A GAUCHE
    console.log('clique detecte a gauche de case', cellIndex, 'et', cellIndex - 1, '(colonne', realX, 'et', realX + 1 + ')');
    affectedCells.push(cellIndex - 1);
    // AJOUTE DE LA 4E CASE EN CAS DE CIQUE  DANS LE COIN
    if (onTop) { affectedCells.push(cellIndex - NB_VISIBLE_CELL_X - 1); }
    if (onBottom) { affectedCells.push(cellIndex + NB_VISIBLE_CELL_X - 1); }
  }

  // SI CLIQUE SUR PETIT CARRE AVEC detectedY >= 9
  if (detectedY >= NB_DETECTABLE_CELL_PER_VISIBLE_CELL_Y - MULTI_CELL_DETECTION_RANGE) {
    // ALORS CLIQUE DETECTER ENTRE LA CASE CLIQUE A DROITE
    console.log('clique detecte a droite de case', cellIndex, 'et', cellIndex + 1, '(colonne', realX + 1, 'et', realX + 2 + ')');
    affectedCells.push(cellIndex + 1);
    // AJOUTE DE LA 4E CASE EN CAS DE CIQUE  DANS LE COIN
    if (onTop) { affectedCells.push(cellIndex - NB_VISIBLE_CELL_X + 1); }
    if (onBottom) { affectedCells.push(cellIndex + NB_VISIBLE_CELL_X + 1); }
  }

  // FITLER POUR AVOIR QUE DES VALEUR UNIQUE
  affectedCells = affectedCells.filter((value, index, self) => self.indexOf(value) === index);
  console.log('case affecte par le clic:', affectedCells);
};

type VisibleCellProps = {
  realX: number;
  realY: number;
};

const VisibleCell: React.FC<VisibleCellProps> = ({realX, realY}) => {
  const cellIndex = (realY * NB_VISIBLE_CELL_Y) + realX + 1;
  return (
    <div className='visible-cell'>
      <p className='cell-index'>{cellIndex}</p>
      {/* LOOP DES Y POUR LES CARRES CLIQUABLE */}
      {[...Array(NB_DETECTABLE_CELL_PER_VISIBLE_CELL_Y)].map((e, x) => {
        return (
          <div className='line'>
            {/* LOOP DES X POUR LES CARRES CLIQUABLE */}
            {[...Array(NB_DETECTABLE_CELL_PER_VISIBLE_CELL_X)].map((e, y) => {
              return (
                // LE PETIT CARRE CLIQUABLE
                <div  className="detectable-cell"
                      key={(cellIndex * 100) + (y * 10) + x}
                      data-x={x+1}
                      data-y={y+1}
                      onClick={() => fonctionQuiFaitLesMaths(realX, realY, x, y)}
                />
              );
            })}
          </div>
        );
      })}
    </div>
  );
}

function App() {
  return (
    <div className='main-container'>
      <div className='main-table'>
        {/* LOOP DES Y POUR LES CARRES BLANC */}
        {[...Array(NB_VISIBLE_CELL_Y)].map((e, y) => {
          return (
              <div key={'visible-line-' + y} className='visible-line'>
                {/* LOOP DES X POUR LES CARRES BLANC */}
                {[...Array(NB_VISIBLE_CELL_X)].map((e, x) => {
                  return (<VisibleCell key={'visible-cell-' + x + '-' + y} realX={x} realY={y}/>);
                })}
              </div>
          );
        })}
      </div>
    </div>
  );
}

export default App;
